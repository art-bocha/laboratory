import { LaboratoryPage } from './app.po';

describe('laboratory App', () => {
  let page: LaboratoryPage;

  beforeEach(() => {
    page = new LaboratoryPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
