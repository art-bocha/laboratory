import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email: string;
  password: string;
  errorMessage: string;

  constructor(public authService: AuthService, private router:Router) { 
    console.log("LoginComponent constructor");
    this.authService.loggedIn$.subscribe(isLoggedIn => {
        if(isLoggedIn)
        { 
            this.router.navigate(['/task']);
        }
    })
  }

  ngOnInit() {
    this.errorMessage = null;
  }

  onLogin() {
    if (!this.email || !this.password) {
      this.errorMessage = "All fields are required";
      return;
    }
    
    this.errorMessage = null;
    if(!this.authService.login(this.email, this.password))
      this.errorMessage = "Invalid email address or password";
  }
}