import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { RegistrationUser } from '../../models/user';

@Component({
  selector: 'app-registry',
  templateUrl: './registry.component.html',
  styleUrls: ['./registry.component.css']
})
export class RegistryComponent implements OnInit {
  registrationUser: RegistrationUser;
  errorMessage: string;

  constructor(public authService: AuthService, private router:Router) { }

  ngOnInit() {
    this.registrationUser = new RegistrationUser();
    this.errorMessage = null;
  }

  onRegister() {
    this.errorMessage = null;
    this.authService.signUp(this.registrationUser);
    this.router.navigate(['task']);
  }

}
