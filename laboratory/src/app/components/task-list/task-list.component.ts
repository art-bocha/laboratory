import { Component, OnInit, Input } from '@angular/core';

import {Task} from '../../../app/models/task';
import {TaskService} from '../../services/task.service'

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})

export class TaskListComponent implements OnInit {
  @Input() tasks: Task[];

  constructor(private taskService: TaskService) {
    this.tasks = [];
  }

  ngOnInit() {
  }
}
