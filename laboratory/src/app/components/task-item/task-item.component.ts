import { Component, OnInit, Input } from '@angular/core';

import {Task} from '../../../app/models/task';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-task-item',
  templateUrl: './task-item.component.html',
  styleUrls: ['./task-item.component.css']
})

export class TaskItemComponent implements OnInit {
  @Input() task: Task;

  constructor(private authService:AuthService) { }

  ngOnInit() {
  }
}