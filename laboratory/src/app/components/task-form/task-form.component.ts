import { Component, OnInit} from '@angular/core';

import {Task} from '../../../app/models/task';
import {TaskService} from '../../services/task.service';

@Component({
  selector: 'app-task-form',
  templateUrl: './task-form.component.html',
  styleUrls: ['./task-form.component.css']
})

export class TaskFormComponent implements OnInit {
  title: string = '';

  constructor(private taskService: TaskService) { }

  ngOnInit() {
  }
  
  onSubmit() {
    // this.taskService.addTask(this.title);
  }
}
