import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { AppComponent } from './app.component';
import { TaskListComponent } from './components/task-list/task-list.component';
import { TaskFormComponent } from './components/task-form/task-form.component';
import { TaskItemComponent } from './components/task-item/task-item.component';
import { TaskService } from './services/task.service';
import { DataService } from './services/data.service';
import { LoginComponent } from './components/login/login.component';
import { TaskStatusComponent } from './components/task-status/task-status.component';
import { MaxTextLengthPipe } from './pipes/max-text-length.pipe';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './guards/AuthGuard';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { TaskPageComponent } from './pages/task-page/task-page.component';
import { TaskListPageComponent } from './pages/task-list-page/task-list-page.component';
import { ProfilePageComponent } from './pages/profile-page/profile-page.component';
import { TaskStatusListComponent } from './components/task-status-list/task-status-list.component';
import { RouterModule } from '@angular/router';
import { appRoutes } from './app.routes';
import { RegistryComponent } from './components/registry/registry.component';
import { RegistryPageComponent } from './pages/registry-page/registry-page.component';

@NgModule({
  declarations: [
    AppComponent,
    TaskListComponent,
    TaskFormComponent,
    TaskItemComponent,
    LoginComponent,
    HomePageComponent,
    LoginPageComponent,
    TaskPageComponent,
    TaskListPageComponent,
    ProfilePageComponent,
    TaskStatusComponent,
    TaskStatusListComponent,
    MaxTextLengthPipe,
    RegistryComponent,
    RegistryPageComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    //InMemoryWebApiModule.forRoot(DataService)
  ],
  providers: [TaskService, AuthService, AuthGuard],
  bootstrap: [AppComponent]
})

export class AppModule { }