import { Routes } from '@angular/router';
import { TaskListPageComponent } from './pages/task-list-page/task-list-page.component';
import { TaskPageComponent } from './pages/task-page/task-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { AuthGuard } from './guards/AuthGuard';
import { ProfilePageComponent } from './pages/profile-page/profile-page.component';
import { RegistryPageComponent } from './pages/registry-page/registry-page.component';

export const appRoutes: Routes = [
    { path: '', component: HomePageComponent, pathMatch: 'full' },
    { path: 'task/:id', component: TaskPageComponent, canActivate: [AuthGuard] },
    { path: 'task', component: TaskListPageComponent},
    { path: 'login', component: LoginPageComponent},
    { path: 'profile', component: ProfilePageComponent},
    { path: 'registry', component: RegistryPageComponent },
    { path: '**', redirectTo: "" }
];