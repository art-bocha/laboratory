import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { Observable } from 'rxjs';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private userService: AuthService, private router:Router) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):Observable<Boolean> {
        return this.userService.auth$.map( (user) => {
            console.log("can active");
            if(user) return true;
            else {
                this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
                return false;
            }
        });
    }
}