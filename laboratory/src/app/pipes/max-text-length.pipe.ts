import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'maxTextLength'
})
export class MaxTextLengthPipe {
  transform(value: string, maxLength:number) : string {

    let outVal:string = value.substring(0,maxLength);

    if(outVal.length < value.length)
    {
      outVal.concat("...");
    }

    return outVal;
  }
}