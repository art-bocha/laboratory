import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';

@Injectable()
export class DataService implements InMemoryDbService{

  constructor() { }

  createDb() {
    const tasks = [
      {
        title: "C#",
        pathFile: ""
      },
      {
        title: "JavaScript",
        pathFile: ""
      },
      {
        title: "Python",
        pathFile: ""
      }
    ];

    return { tasks };
  }

}