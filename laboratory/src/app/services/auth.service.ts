import { Injectable } from '@angular/core';
import { User, RegistrationUser } from "../models/user";
import { Observable, BehaviorSubject } from 'rxjs/Rx';
import { DBUser } from '../models/dbUser';

@Injectable()
export class AuthService {
  private authSubj:BehaviorSubject<User> = new BehaviorSubject(null);
  private usersDB:DBUser[];

  public auth$:Observable<User> = this.authSubj.asObservable();
  public loggedIn$:Observable<boolean> = this.auth$.map( (user) => user != null);

  constructor() { 
    this.usersDB = [
      new DBUser("Artyom", "art@mail.com", "1006411")
    ];
    this.logout();
    const user = JSON.parse(window.localStorage.getItem("auth")) as User;
    this.authSubj.next(user);
  }

  login(email:string, password:string) {
    const auth = this.usersDB.filter((user:DBUser) => {
      return (user.email === email && user.password === password);
    });

    if(auth.length > 0) {
      const user:User = new User(auth[0].name, auth[0].email);
      window.localStorage.setItem("auth", JSON.stringify(user));
      this.authSubj.next(user);
    }
    else
    {
      return;
    }
  }

  logout() {
    this.authSubj.next(null);
    window.localStorage.setItem("auth", JSON.stringify(null));
  }

  signUp(user:RegistrationUser):Observable<any> {
    const dbUser:DBUser = new DBUser(user.name, user.email, user.password);
    this.usersDB.push(dbUser);
    
    this.authSubj.next(dbUser);
    return Observable.of({status: "created"});
  }
}