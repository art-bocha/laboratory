import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { Task, ITask } from 'app/models/task';
import { Observable } from 'rxjs';
import { ITaskStatus } from '../models/task_status';
import { AuthService } from './auth.service';

@Injectable()
export class TaskService {
  tasks: Task[] = [];
  //private apiUrl = 'http://localhost:3000/task';
  private apiUrl = 'https://1eheecnoa2.execute-api.us-west-2.amazonaws.com/test/task';

  constructor(private http: Http) {}

  getTasks(): Observable<ITask[]> {
    return this.http.get(this.apiUrl)
                    .map(res => res.json() as ITask[]);
  }

  getTaskById(id:string):Observable<ITask>
  {
    return this.http.get(this.apiUrl+"/"+id).do(console.log)
        .map(res => res.json() as ITask);
  }

  /*getTaskStatuses():ITaskStatus
  {
    return this.authService.auth$.switchMap( (user) => {
      return this.http.get(this.)
    });
  }*/

  // addTask(title: string) {
  //   let headers = new Headers({ 'Content-Type': 'application/json' });
  //   let options = new RequestOptions({ headers })
  //   let task: Task = new Task(title);

  //   this.http.post(this.apiUrl, task, options)
  //            .toPromise()
  //            .then(res => res.json())
  //            .then(task => this.tasks.push(task))
  //            .catch(this.handlerError)
  // }

  // private handlerError(err: any) {
  //   console.log("Произошла ошибка");
  //   return Promise.reject(err.message || err);
  // }
}