import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ITask } from '../../models/task';
import { TaskService } from '../../services/task.service';

@Component({
  selector: 'app-tasks-list-page',
  templateUrl: './task-list-page.component.html',
  styleUrls: ['./task-list-page.component.css']
})
export class TaskListPageComponent implements OnInit {
  tasks$:Observable<ITask[]>;

  constructor(private taskService:TaskService) { 
    this.tasks$ = this.taskService.getTasks();
  }

  ngOnInit() {
  }

}