import { Component, OnInit } from '@angular/core';
import {  ActivatedRoute  } from '@angular/router';
import {  TaskService } from '../../services/task.service';
import {  Observable  } from 'rxjs';
import {  ITask } from '../../models/task';

@Component({
  selector: 'app-task-page',
  templateUrl: './task-page.component.html',
  styleUrls: ['./task-page.component.css']
})
export class TaskPageComponent implements OnInit {
  task$:Observable<ITask>;
  constructor(private route: ActivatedRoute, private taskService:TaskService) { }

  ngOnInit() {
    this.task$ = this.route.params.do(console.log).switchMap((params) => {
      const id = params["id"];
      console.log(id);
      return this.taskService.getTaskById(id);
    }).do(console.log);

    //this.task$.subscribe();
  }

  submitTask()
  {

  }
}