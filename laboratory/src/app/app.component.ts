import { Component } from '@angular/core';

import { Task } from '../app/models/task';
import { AuthService } from './services/auth.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title: string = 'Список задач';
  loggedIn$:Observable<boolean>;

  constructor(private authService:AuthService, private router:Router)
  {
    this.loggedIn$ = this.authService.loggedIn$;
  }

  logOut()
  {
    this.authService.logout();
    this.router.navigate(['']);
  }
}
