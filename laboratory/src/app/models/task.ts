export interface ITask
{
    title:string;
    description:string;
    filePath:string;
    id:string;
}

export class Task implements ITask{
    public id:string;
    
    constructor(public title:string, public description:string, public filePath: string = "") {}
}