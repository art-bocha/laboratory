export interface ITaskStatus {
    id:string;
    name:string;
    status:string;
    result:number;
}