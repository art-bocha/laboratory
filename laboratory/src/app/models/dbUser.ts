import { User } from './user';

export class DBUser extends User {
    constructor(public name:string,
                public email:string,
                public password:string) 
    {
        super(name, email)
    }
}