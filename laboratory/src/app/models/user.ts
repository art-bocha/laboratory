export class User {
    id: string;

    constructor(public name:string,
                public email:string) {}
}

export class RegistrationUser {
    constructor(public name:string = "",
                public email: string = "",
                public password: string = "") {}
}