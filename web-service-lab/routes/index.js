var express = require('express');
var router = express.Router();

const tasks = [
  {
    id: "0",
    description: "Lorem ipsum dsjh adkjba ajb sciud sbjhd",
    title: "Lokomotiv",
    pathFile: "/sdfa"
  },
  {
    id: "1",
    description: "Lorem ipsum dsjh adkjba ajb sciud sbjhd",
    title: "Dinamo",
    pathFile: "/Moskva"
  },
  {
    id: "2",
    description: "Lorem ipsum dsjh adkjba ajb sciud sbjhd",
    title: "Akbars",
    pathFile: "Kazan"
  }
];
/* GET home page. */
router.get('/', function(req, res, next) {
  res.json(tasks);
});

router.get('/:id', function(req, res, next) {
    res.json(tasks[req.params.id]);
});

router.post('/', function(req, res, next) {
  let task = req.body;

  if (!task.title) {
    res.status(400);
    res.json({
      "error": "Bad Data"
    });
  }
  tasks.push(task);
  
  res.json(task);
});

module.exports = router;
